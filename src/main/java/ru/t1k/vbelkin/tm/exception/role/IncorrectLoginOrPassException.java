package ru.t1k.vbelkin.tm.exception.role;

public class IncorrectLoginOrPassException extends AbstractUserException {

    public IncorrectLoginOrPassException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}

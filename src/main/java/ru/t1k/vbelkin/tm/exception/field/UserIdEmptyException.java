package ru.t1k.vbelkin.tm.exception.field;

public class UserIdEmptyException extends AbstractFieldException {
    public UserIdEmptyException() {
        super("Error! User id is empty...");
    }
}


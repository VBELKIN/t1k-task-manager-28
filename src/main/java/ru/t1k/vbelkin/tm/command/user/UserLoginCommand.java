package ru.t1k.vbelkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "login";

    @NotNull
    private static final String DESCRIPTION = "user login";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }


}
